<?php
/**
 * Field handler to present a link to delete a node.
 */
class node_archive_handler_field_link_restore extends views_handler_field_node_link {

  /**
   * Renders the link.
   */
  function render_link($node, $values) {
    // Ensure user has access to restore this node.
    if (!user_access('restore archived nodes')) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "node/$node->nid/restore";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('Restore');
    return $text;
  }
}

