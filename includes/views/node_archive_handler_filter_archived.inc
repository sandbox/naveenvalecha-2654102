<?php

/**
 * @file
 *   Filters statuses to a certain recipient type.
 */

/**
 * Filter handler to select statuses with a given recipient type.
 */

class node_archive_handler_filter_archived extends views_handler_filter {
  function admin_summary() { }
  function query() {
    $this->ensure_my_table();
    if ($this->value[0] == 'yes') {
      $this->query->add_where($this->options['group'], "node.nid IN (SELECT nid FROM {node_archive})");
    }
    elseif ($this->value[0] == 'no') {
      $this->query->add_where($this->options['group'], "node.nid NOT IN (SELECT nid FROM {node_archive})");
    }
  }

  /**
   * Provide simple equality operator
   */
  function operator_form(&$form, &$form_state) {
    $form['operator'] = array(
      '#type' => 'radios',
      '#title' => t('Archived'),
      '#description' => t('Choose whether to show archived or normal nodes.'),
      '#default_value' => $this->operator,
      '#options' => array(
        0 => t('All'),
        'yes' => t('Yes'),
        'no' => t("No"),
      ),
    );
  }

  /**
   * Provide a simple textfield for equality
   */
  function exposed_form(&$form, &$form_state) {
    if (isset($this->options['expose']['identifier'])) {
      $key = $this->options['expose']['identifier'];
      $form[$key] = array(
        '#type' => 'select',
        '#default_value' => $this->value,
        '#options' => array(
          0 => t('<Any>'),
          'yes' => t('Yes'),
          'no' => t("No"),
        ),
      );
    }
  }

}
