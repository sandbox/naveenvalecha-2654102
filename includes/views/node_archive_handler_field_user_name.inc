<?php
/**
 * Field handler to provide simple renderer that allows using a themed user link
 */
class node_archive_handler_field_user_name extends views_handler_field_user_name {
  function render($values) {
    if (!$values->{$this->field_alias}) {
      return;
    }

    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }
}

