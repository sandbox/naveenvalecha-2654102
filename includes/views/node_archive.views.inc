<?php

/**
 * @file
 * Provide views data and handlers for the Facebook-style Statuses module.
 */

/**
 * Implements hook_views_data().
 */
function node_archive_views_data() {

  $data = array();

  $data['node_archive']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Node Archive'),
    'help' => t('Display a list of archived nodes.'),
    'weight' => -10,
  );

  $data['node_archive']['table']['group'] = t('Node Archive');

  $data['node_archive']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['node_archive']['nid'] = array(
    'title' => t('Archived'),
    'help' => t('Has the node been archived.'),
    'field' => array(
      'handler' => 'node_archive_handler_field_archived',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('Archived Nodes'),
    ),
    'filter' => array(
      'handler' => 'node_archive_handler_filter_archived',
    ),
  );

  $data['node']['restore_node']['moved to'] = array('views_entity_node', 'restore_node');
  $data['views_entity_node']['restore_node'] = array(
    'field' => array(
      'title' => t('Restore link'),
      'help' => t('Provide a simple link to restore the node.'),
      'handler' => 'node_archive_handler_field_link_restore',
    ),
  );

  $data['node']['node_archive'] = array(
    'title' => t('Archived'),
    'help' => t('Ensure the selected nodes have not been archived.'),
    'filter' => array(
      'handler' => 'node_archive_handler_filter_archived',
    ),
  );

  $data['node_archive']['timestamp'] = array(
    // The item it appears as on the UI.
    'title' => t('Archived date'),
    // The help that appears on the UI.
    'help' => t('The date the node was archived.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['node_archive']['uid'] = array(
    // The item it appears as on the UI.
    'title' => t("Archiver's name"),
    // The help that appears on the UI.
    'help' => t('The user who archived the node.'),
    'field' => array(
      'handler' => 'node_archive_handler_field_user_name',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}
