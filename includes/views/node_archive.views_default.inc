<?php

/**
 * @file
 * Declares our default views to the views module
 */

/**
 * Implements of hook_views_default_views().
 */
function node_archive_views_default_views() {
  $files = file_scan_directory(drupal_get_path('module', 'node_archive') . '/includes/views/default', '/.view/');
  foreach ($files as $absolute => $file) {
    require DRUPAL_ROOT . '/' . $absolute;
    if (isset($view)) {
      $views[$file->name] = $view;
    }
  }
  return $views;
}
