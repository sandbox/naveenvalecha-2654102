***********
* README: *
***********

DESCRIPTION:
------------
This module replaces this default behavior with an archiving system so that 
instead of being deleted nodes are “archived” and can later be “restored”.

This module was developed for the Government of British Columbia’s public 
service intranet known as @Work. FOI and data retention policies demand that 
anything posted to this website should be accessible in the future.

Note that this module does not and cannot prevent node deletions entirely. 
An admin can still find ways to delete nodes. What this module does is 
over-write the behavior of node/%/delete as well as the default node edit 
buttons. For administrators, the content management page at admin/content/node 
has been over-written to allow for filtering of archived nodes and archiving 
and restoring bulk operations.

Additionally, views support is included for the ability to list archived nodes 
and conduct actions with VBO.


REQUIREMENTS:
-------------
views


INSTALLATION:
-------------
  *	Install the module as any drupal module.
  *	Visit permissions to assign “view archived nodes” and “restore archived nodes”.
  *	Check settings at admin/settings/node-archive.
  *	If using VBO to manage your content add the archive and restore operations 
    under the “Selected Operations” in your view.


Features:
---------
  *	The terms “archive”, “archived”, “restore” and “restored” can be customized.
  *	Node revisioning can be enforced so all node edits are automatically saved.
  *	Prevent any node revisions from being deleted.
  *	When a node is archived options are presented for what to do with any 
    related menu items – they may be left as-is, hidden or deleted.
  *	When a node is archived you can automatically delete the path alias.
  *	The above options are available per node but can also be set for all bulk 
    operations.


Note:
-----
  *	Permissions can be granted to view archived nodes and to restore them. 
    The ability to archive a node is extended from the delete permission since 
    the operation is done at node/%/delete.
  *	Nodes are automatically unpublished when archived.


Author:
-------
Saren Calvert:
Sarenc on drupal.org